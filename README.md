# LaTeX Templates and Resources

## My recommended setup with vscode

[TeX Live](https://tug.org/texlive/windows.html): LaTeX

[vscode](https://code.visualstudio.com/): Editor

**vscode extensions:**

[LaTeX Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop): The extension to edit and compile LaTeX projects

[LaTeX Utilities](https://marketplace.visualstudio.com/items?itemName=tecosaur.latex-utilities): more features for LaTeX workshop

[LTeX](https://marketplace.visualstudio.com/items?itemName=valentjn.vscode-ltex): grammar and spell checking

[LaTeX language support](https://marketplace.visualstudio.com/items?itemName=torn4dom4n.latex-support): for syntax highlighting and formatting

**bibliography:**

[Zotero](https://www.zotero.org/): bibliography manager

[better bibtex for zotero](https://retorque.re/zotero-better-bibtex/): improvements for Zotero, e.g. automatic .bib file export

## Advanced

[VSCode-LaTeX-Inkscape](https://github.com/sleepymalc/VSCode-LaTeX-Inkscape): If you want to write LaTeX really fast. Tools for snippets, conceal, fast image drawing


## Links to valuable resources

[CTAN](https://ctan.org/): LaTeX database with all packages

[learnlatex](https://www.learnlatex.org/): for learning

[Overleaf](https://de.overleaf.com/learn): nice reference for basic stuff, online editor

[Golatex](https://golatex.de/wiki/index.php/Hauptseite): german reference

[Wikibooks](https://en.wikibooks.org/wiki/LaTeX): good reference

[Komaskript](https://komascript.de/): about KOMA-Scripts which are commonly used in germany/europe

[Latextemplates Github](https://latextemplates.github.io/): templates

[Latextemplates](http://www.latextemplates.com/): More templates

[Awesome Latex Github](https://github.com/egeerardyn/awesome-LaTeX): nice list with everything you need

[Tables Generator](https://www.tablesgenerator.com/#) generates tables (sadly no tabularx)

[TeXcount web service](https://app.uio.no/ifi/texcount/online.php) word counter (LaTeX utilities does this, too)

## Manuals and documentation
PDFs in the repo
